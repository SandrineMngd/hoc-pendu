import React from 'react';
// import logo from './logo.svg';
import './App.css';
import alphabet from './alphabet.json';

function App() {

// Afficher les vies restantes : 1 clic faux = 1 vie en moins / 1 clic vrai = 0 vie en moins
// 5 vies - compteur décrémenté au fur et à mesure des clics - compteur à 0 = message "perdu"
// Afficher les lettres du mot au fur et à mesure qu'on le découvre
// Afficher la barre des lettres : 
// 1 clic (vrai ou faux) désactive la case
// 1 clic faux = 1 vie en moins 
// 1 clic vrai = 0 vie en moins et la lettre s'affiche
// Mot trouvé = message "gagné"

function displayLetters(){
  return alphabet.map((lettre, id) => <input type="button" value={lettre} key={id} onClick={disableButton}/>)
}

function disableButton(event) {
    const btn = event.target;
    btn.disabled = true;
}




return(
<div><center>

  <div>
    <h2>Jeu du Pendu</h2>
  </div>

  <div>
    <h3>Nombre de vies restantes : * * * * * </h3>
  </div>

  <div>
    <h2>_ _ _ _ _ _</h2>
  </div>

  <div>
  <div>{displayLetters()}</div>
  </div>

</center></div>
);
}

export default App;
